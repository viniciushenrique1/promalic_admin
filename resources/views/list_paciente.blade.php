@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
<?php 
  $arrayComQuemMora = [
    1 => "Sozinho",
    2 => "Companheiro(a)",
    3 => "Filhos(as)",
    4 => "Familiares",
    5 => "Outros"
  ];

  $arrayPossuiDoenca = [
    1 => "Sim",
    0 => "Não"
  ];
?>

<div class="row">
          <div class="col-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Listagem de Pacientes</h3>

                <div class="card-tools" style="display: inline-flex;">
                <div class="input-group input-group-sm" style="width: 40px;">
                  
                    <div class="input-group-append">
                      <button id="btn-print" type="button" class="btn btn-default" style="border-top-left-radius: 3px; border-bottom-left-radius: 3px;"><i class="fas fa-print"></i></button>
                    </div>
                    
                  </div>
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover" id="printTable">
                  <thead>
                    <tr>
                      <th style="width: 100px;"></th>
                      <th>ID</th>
                      <th>Nome</th>
                      <th>Sobrenome</th>
                      <th>Email</th>
                      <th>Nascimento</th>
                      <th>Possui Doença</th>
                      <th>Com Quem Mora</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($users as $user)
                    <tr>
                      <td><button onclick="location.href='pacientes_deletar/<?= $user->user ?>/<?= $user->id ?>'" type="button" class="btn btn-danger  btn-sm">Excluir</button></td>
                      <td onclick="location.href='edit_paciente/<?= $user->user ?>'"><?= $user->user ?></td>
                      <td onclick="location.href='edit_paciente/<?= $user->user ?>'"><?= $user->name ?></td>
                      <td onclick="location.href='edit_paciente/<?= $user->user ?>'"><?= $user->sobrenome ?></td>
                      <td onclick="location.href='edit_paciente/<?= $user->user ?>'"><?= $user->email ?></td>
                      <td onclick="location.href='edit_paciente/<?= $user->user ?>'"><?= $user->nascimento ?></td>
                      <td onclick="location.href='edit_paciente/<?= $user->user ?>'"><?= $arrayPossuiDoenca[$user->possui_doenca] ?></td>
                      <td onclick="location.href='edit_paciente/<?= $user->user ?>'"><?= $arrayComQuemMora[$user->com_quem_mora] ?></td>
                    </tr>
                      @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
@stop

@section('js')
<script>
  function printData()
  {
    var divToPrint=document.getElementById("printTable");
    newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
  }

  $('#btn-print').on('click', function(){
    printData();
  });
</script>
@stop