@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
@section('title', 'Laudos')
<h1>Laudo Emitido</h1>
@stop

@section('content')
<div style="padding: 0px 15px 15px 15px !important;">

  @if(empty($reports))
  <div class="col-md-12">
    <h4>Não existe laudo emitido para este paciente.</h4>
  </div>
  @else
  <div class="row pb-3">
    <div class="col-md-12">
      <div class="input-group input-group-sm" style="width: 110px;">
          <button id="btn-print" type="button" class="btn btn-default" style="border-top-left-radius: 3px; border-bottom-left-radius: 3px;"><i class="fas fa-print"></i>&nbsp;&nbsp;Imprimir</button>
      </div>
    </div>
  </div>
  <div id="printTable">
  @foreach ($reports as $report)
    <div class="row">
      <div class="col-md-12">
        <h4>Preenchido por: {{$report['user']['name']}}&nbsp;{{ isset($report['user']['attrp']) ? $report['user']['attrp']['sobrenome'] ?? '' : '' }}&nbsp;({{ isset($report['user']['attrp']['espec']) ? $report['user']['attrp']['espec']['nome'] ?? '' : '' }})</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <h4>{{$report['created_at_ptbr']}}</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <h5>
        @foreach ($report['answers'] as $answer)
          @if($answer['item']['type'] == 'checkbox')
            <p><i class="fas fa-check" style="color:grey;"></i>&nbsp;&nbsp;&nbsp;{{$answer['item']['item']}}</p>
          @elseif($answer['item']['type'] == 'textarea')
            <p><i class="fas fa-pencil-alt" style="color:grey;"></i>&nbsp;&nbsp;&nbsp;{{$answer['textarea']}}</p>
          @endif
          @endforeach
        </h5>
      </div>
    </div>
    @endforeach
  </div>
  @endif

</div>
@stop

@section('js')
<script>
  function printData()
  {
    var divToPrint=document.getElementById("printTable");
    newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
  }

  $('#btn-print').on('click', function(){
    printData();
  });
</script>
@stop