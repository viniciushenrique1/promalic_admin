@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
@section('title', 'Consultar Laudos')
<h1>Laudos Emitidos</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12 pb-3">
      <div class="input-group input-group-sm" style="width: 110px;">
          <button id="btn-print" type="button" class="btn btn-default" style="border-top-left-radius: 3px; border-bottom-left-radius: 3px;"><i class="fas fa-print"></i>&nbsp;&nbsp;Imprimir</button>
      </div>
    </div>

  <div class="col-md-12" id="printTable">
    <div class="timeline">
      @foreach ($reports as $report)
          <div class="time-label">
            <span class="bg-green">{{$report['created_at_ptbr']}}</span>
          </div>
          
          <div>
            <i class="fas fa-user bg-green"></i>
            <div class="timeline-item">
              <h5 class="p-3">Preenchido por: {{$report['user']['name']}}&nbsp;{{ isset($report['user']['attrp']) ? $report['user']['attrp']['sobrenome'] ?? '' : '' }}&nbsp;({{ isset($report['user']['attrp']['espec']) ? $report['user']['attrp']['espec']['nome'] ?? '' : '' }})</h5>
              <h3 class="timeline-header no-border">
              @foreach ($report['answers'] as $answer)
                @if($answer['item']['type'] == 'checkbox')
                  <p><i class="fas fa-check" style="color:yellow;"></i>&nbsp;&nbsp;&nbsp;{{$answer['item']['item']}}</p>
                @elseif($answer['item']['type'] == 'textarea')
                  <p><i class="fas fa-pencil-alt" style="color:orange;"></i>&nbsp;&nbsp;&nbsp;{{$answer['textarea']}}</p>
                @endif
                @endforeach
              </h3>
            </div>
          </div>

      @endforeach
      <div>
        <i class="fas fa-clock bg-gray"></i>
      </div>
    </div>
  </div>
</div>
@stop

@section('js')
<script>
  function printData()
  {
    var divToPrint=document.getElementById("printTable");
    newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
  }

  $('#btn-print').on('click', function(){
    printData();
  });
</script>
@stop