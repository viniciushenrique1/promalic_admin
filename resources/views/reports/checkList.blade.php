@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
<div class="card card-info">
  <div class="card-header">
    <h3 class="card-title">Novo Laudo Médico</h3>
  </div>
  <form action="{{ url('report-check-list-save') }}" method="post" class="form-horizontal">
    @csrf
    <input value="{{$paciente_id}}" name="paciente_id" type="hidden">
    <div class="card-body">
     <div class="row">
      <div class="col-sm-12">
        <div class="container">

        @foreach($questions as $question)
            @if(sizeof($question['subquestions']) > 0)
                <h4 style="margin-top: .5rem;">{{$question['question']}}</h4>
                @foreach($question['subquestions'] as $subquestion)
                    <div style="padding: 5px;">
                        <button type="button" class="btn btn-outline-info" data-toggle="collapse" data-target="#collapse{{$question['id']}}{{$subquestion['id']}}">
                            {{$subquestion['subquestion']}}
                        </button>
                    </div>
                    <div id="collapse{{$question['id']}}{{$subquestion['id']}}" class="collapse">
                    @foreach($subquestion['items'] as $item)
                        @if($item['type'] == 'checkbox')
                        <input type="hidden" value="0" name="items[{{$item['id']}}]">
                        <div class="checkbox">
                            <label><input name="items[{{$item['id']}}]" type="checkbox" value="1"> {{$item['item']}}</label>
                        </div>
                        @elseif($item['type'] == 'textarea')
                        <label>{{$item['item']}}</label>
                        <textarea name="items[{{$item['id']}}]" class="form-control" rows="5"></textarea>
                        @endif
                    @endforeach
                    </div>
                @endforeach
            @endif
        @endforeach

        </div>
      </div>
    </div>
  </div>
</div>

  <div class="col-sm-12 p-0">
    <button type="submit" class="btn btn-info form-control">Inserir</button>
  </div>

</form>
</div>
@stop

@section('css')
<style>
  #pergunta_2, #pergunta_1{
    padding: 10px;
    border: solid 1px;
    margin: 10px;
  }
</style>
@stop