@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
<div class="card card-info">
  <div class="card-header">
    <h3 class="card-title">Novo Laudo Médico - Seleção de items</h3>
  </div>
  <form action="{{ url('report-check-list') }}" method="post" class="form-horizontal">
    @csrf
    <input value="{{$paciente_id}}" name="paciente_id" type="hidden">
    <div class="card-body">
     <div class="row">
      <div class="col-sm-12">
        <div class="container">
            
            @foreach($questions as $question)
              @if(sizeof($question['subquestions']) > 0)

                <h5>{{$question['question']}}</h5>
                @foreach($question['subquestions'] as $subquestion)
                  <div class="checkbox">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label><input name="subquestions[]" type="checkbox" value="{{$subquestion['id']}}"> {{$subquestion['subquestion']}}</label>
                  </div>
                @endforeach

              @endif
            @endforeach

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <div class="col-sm-12 pb-3">
    <button type="submit" class="btn btn-info form-control">Próximo</button>
  </div>

</form>
</div>
@stop

@section('css')
<style>
  #pergunta_2, #pergunta_1{
    padding: 10px;
    border: solid 1px;
    margin: 10px;
  }
</style>
@stop