@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
<div class="card card-info">
  <div class="card-header">
     <!-- <div class="row"> -->
       <!-- <div class="col-sm-2"> -->
         <h3 class="card-title">Editar Paciente</h3>
         <div class="card-tools" style="display: inline-flex;">
            <div class="input-group input-group-sm" style="width: 260px;">
              <div class="input-group-append">
                <!-- <button id="btn-print" type="button" class="btn btn-default" style="border-top-left-radius: 3px; border-bottom-left-radius: 3px;"><i class="fas fa-print"></i></button> -->
                <button onclick="location.href='{{url('report-check')}}/{{$user->user}}'" type="button" class="btn btn-light" style="border-radius: 3px; border-radius: 3px; margin-right: 10px;"><i class="far fa-file-alt"></i>&nbsp;Inserir Laudo</button>
                <button onclick="location.href='{{url('report-view')}}/{{$user->user}}'" type="button" class="btn btn-light" style="border-radius: 3px; border-radius: 3px;"><i class="far fa-file-alt"></i>&nbsp;Visualizar Laudo</button>

              </div>
            </div>
            </div>
       <!-- </div> -->
       <!-- <div class="col-sm-8"></div> -->
       <!-- <div class="col-sm-2"><button onclick="location.href='{{url('new_laudo')}}/{{$user->user}}'" type="button" class="btn btn-light">Inserir Laudo</button></div> -->
       <!-- <div class="col-sm-2"><button onclick="location.href='{{url('report-check')}}/{{$user->user}}'" type="button" class="btn btn-light">Inserir Laudo</button></div> -->
     <!-- </div> -->
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form action="{{ url('pacientes_edit_save') }}" method="post" class="form-horizontal">
    @csrf

    <input value="<?= $user->id ?>" name="attribute_id" type="hidden">
    <div class="card-body">
     <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <input value="<?= $user->name ?>" name="name" type="name" class="form-control" id="name" placeholder="Nome" required>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <input value="{{$user->sobrenome}}" name="sobrename" type="sobrename" class="form-control" id="name" placeholder="Sobrenome" required>
        </div>
      </div> 
    </div>

    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <input value="{{$user->email}}" name="email" type="email" class="form-control" id="inputEmail3" placeholder="Email" required>
        </div>
      </div>
      <div class="col-sm-2 col-form-label">
        <div class="form-group clearfix">
          <label for="radioPrimary1">Data de Nascimento</label>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <input value="{{$user->nascimento}}" name="nascimento" type="date" class="form-control" id="inputEmail3" placeholder="Data de Nascimento">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <div class="form-group">
          <input required name="cpf" class="form-control" placeholder="CPF" value="{{$user->cpf}}">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input value="<?= $user->endereco ?>" name="endereco" class="form-control" id="inputEmail3" placeholder="Endereço">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 col-form-label">
        <div class="form-group clearfix">
          <label for="radioPrimary1"> Possui outras doenças? </label>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="form-group clearfix mt-2">
          <div class="icheck-primary d-inline">
            <input value="1" type="radio" id="radioPrimary1" name="possui_doenca" <?php if ($user->possui_doenca == 1) echo 'checked="checked"'?>>
            <label for="radioPrimary1"> Sim
            </label>
          </div>
          <div class="icheck-primary d-inline">
            <input value="0" type="radio" id="radioPrimary2" name="possui_doenca" <?php if ($user->possui_doenca == 0) echo 'checked="checked"'?>>
            <label for="radioPrimary2"> Não
            </label>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="form-group">
          <input value="{{$user->doenca}}" name="doenca" class="form-control" id="inputEmail3" placeholder="Outras Doenças">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 col-form-label">
        <div class="form-group">
          <label>Com quem mora? </label>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <select name="com_quem_mora" class="form-control select2" style="width: 100%;">
            <option  <?php if ($user->com_quem_mora == 1) echo 'selected="selected"'?> value="1">Sozinho</option>
            <option <?php if ($user->com_quem_mora == 2) echo 'selected="selected"'?> value="2">Companheiro(a)</option>
            <option <?php if ($user->com_quem_mora == 3) echo 'selected="selected"'?> value="3">Filhos(as)</option>
            <option <?php if ($user->com_quem_mora == 4) echo 'selected="selected"'?> value="4">Familiares</option>
            <option <?php if ($user->com_quem_mora == 5) echo 'selected="selected"'?> value="5">Outros</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <input value="{{$user->com_quem_mora_outro}}" name="com_quem_mora_outro" class="form-control" id="inputEmail3" placeholder="Outros">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12 text-danger">
      <p>* A senha padrão do paciente é o CPF</p>
        <!-- <div class="form-group">
          <input  required name="password" type="text" class="form-control" placeholder="Senha">
        </div> -->
      </div>
    </div>

      <div class="col-sm-12 p-0">
        <button type="submit" class="btn btn-info form-control">Salvar</button>
      </div>

  </form>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop