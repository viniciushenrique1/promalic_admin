@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
@section('title', 'Consultar Laudos')
<h1>Laudos Emitidos</h1>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- The time line -->
    <div class="timeline">
      <!-- timeline time label -->
      @foreach ($laudos as $dateCreated => $registry)
        <div class="time-label">
          <span class="bg-green"><?php echo $dateCreated ?></span>
        </div>

         @foreach ($registry as $registries)
            <div>
              <i class="fas fa-user bg-green"></i>
              <div class="timeline-item">
                <h3 class="timeline-header no-border">
                  <p>
                    <?= $registries->checkbox ?>
                  </p>
                  <p>
                    <?= $registries->pergunta_1 ?>
                  </p>
                  <p>
                    <?= $registries->pergunta_2 ?>
                  </p>
                  <p>
                    <?= $registries->pergunta_3 ?>
                  </p>
                  <p>
                    <?= $registries->pergunta_4 ?>
                  </p>
                </h3>
              </div>
            </div>
         @endforeach


      @endforeach
      <div>
        <i class="fas fa-clock bg-gray"></i>
      </div>
    </div>
  </div>
  <!-- /.col -->
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop