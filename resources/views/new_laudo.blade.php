{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
<div class="card card-info">
  <div class="card-header">
    <h3 class="card-title">Novo Laudo Médico</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form action="{{ url('laudos') }}" method="post" class="form-horizontal">
    @csrf
    <input value="<?= Request()->user ?>" name="paciente_id" type="hidden">
    <div class="card-body">
     <div class="row">
      <div class="col-sm-12">
        <div class="container">
          <h4>Conhecimento sobre a doença</h4>
          <button type="button" class="btn btn-outline-info" data-toggle="collapse" data-target="#pergunta_1">Conhecimentos Sobre Insuficiência Cardíaca</button>
          <div id="pergunta_1" class="collapse">
            <div class="checkbox">
              <label><input name="checkbox_1" type="checkbox" value=""> Verificar o conhecimento que eles têm sobre o que é a IC e suas causas</label>
            </div>

            <label>Explicar de maneira simples o funcionamento normal do coração, o que acontece na IC e indicar a possível  causa da IC da pessoa</label>
            <textarea name="pergunta_1" class="form-control" rows="5" id="comment"></textarea>
          </div>
          <br><br>
          <h4>Complicações da IC - Sinais de Alerta</h4>
          <button type="button" class="btn btn-outline-info" data-toggle="collapse" data-target="#pergunta_2">Presença de Edemas</button>
          <div id="pergunta_2" class="collapse">

            <label>Explicar à pessoas e aos familiares por que esse sintoma acontece</label>
            <textarea name="pergunta_2" class="form-control" rows="5" id="comment"></textarea>
            <br>
            <label>Fazer a orientação sobre verificação de peso durante a pesagem diária no hospital</label>
            <textarea name="pergunta_3" class="form-control" rows="5" id="comment"></textarea>
            <br>
            <label>Indicar qual peso seco da pessoa e orienta-la sobre o peso que deve ser mantido</label>
            <textarea name="pergunta_4" class="form-control" rows="5" id="comment"></textarea>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="card-footer row">
  <div class="col-sm-12">
    <button type="submit" class="btn btn-info form-control">Inserir</button>
  </div>
</div>
</form>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">

<style>
  #pergunta_2, #pergunta_1{
    padding: 10px;
    border: solid 1px;
    margin: 10px;
  }
</style>
@stop