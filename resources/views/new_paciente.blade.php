{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
<div class="card card-info">
  <div class="card-header">
    <h3 class="card-title">Novo Paciente</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form action="{{ url('pacientes') }}" method="post" class="form-horizontal">
    @csrf
    <div class="card-body">
     <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <input required name="name" type="name" class="form-control" placeholder="Nome">
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <input required name="sobrenome" type="sobrenome" class="form-control" placeholder="Sobrenome">
        </div>
      </div> 
    </div>

    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <input required name="email" type="email" class="form-control" placeholder="Email">
        </div>
      </div>
      <div class="col-sm-2 col-form-label">
        <div class="form-group clearfix">
          <label for="radioPrimary1">Data de Nascimento</label>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <input required name="nascimento" type="date" class="form-control">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <div class="form-group">
          <input required name="cpf" class="form-control" placeholder="CPF">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input required name="endereco" class="form-control" placeholder="Endereço">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 col-form-label">
        <div class="form-group clearfix">
          <label for="radioPrimary1"> Possui outras doenças? </label>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="form-group clearfix mt-2">
          <div class="icheck-primary d-inline">
            <input value="1" type="radio" id="radioPrimary1" name="possui_doenca">
            <label for="radioPrimary1"> Sim
            </label>
          </div>
          &nbsp;
          <div class="icheck-primary d-inline">
            <input value="0" type="radio" id="radioPrimary2" name="possui_doenca" checked>
            <label for="radioPrimary2"> Não
            </label>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="form-group">
          <input name="doenca" class="form-control" placeholder="Outras Doenças">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 col-form-label">
        <div class="form-group">
          <label>Com quem mora? </label>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <select name="com_quem_mora" class="form-control select2" style="width: 100%;">
            <option value="1" selected="selected">Sozinho</option>
            <option value="2">Companheiro(a)</option>
            <option value="3">Filhos(as)</option>
            <option value="4">Familiares</option>
            <option value="5">Outros</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <input name="com_quem_mora_outro" class="form-control" placeholder="Outros">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12 text-danger">
      <p>* A senha padrão do paciente é o CPF</p>
        <!-- <div class="form-group">
          <input  required name="password" type="text" class="form-control" placeholder="Senha">
        </div> -->
      </div>
    </div>

      <div class="col-sm-12 p-0">
        <button type="submit" class="btn btn-info form-control">Cadastrar</button>
      </div>

  </form>
</div>
@stop
