@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
<div class="card card-info">
  <div class="card-header">
    <h3 class="card-title">Novo Profissional</h3>
  </div>
  <form action="{{ url('profissional') }}" method="post" class="form-horizontal">
    @csrf
    <div class="card-body">
     <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <input required name="name" type="text" class="form-control" id="name" placeholder="Nome" required>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <input required name="sobrenome" type="text" class="form-control" id="name" placeholder="Sobrenome" required>
        </div>
      </div> 
    </div>

    <div class="row">
      <div class="col-sm-8">
        <div class="form-group">
          <select name="especialidade" class="form-control select2" style="width: 100%;">
            @foreach($especialidades as $espec)
            <option value="{{$espec['id']}}">{{$espec['nome']}}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <input required name="crm" type="text" class="form-control" id="name" placeholder="CRM" required>
        </div>
      </div> 
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input required name="email" type="email" class="form-control" id="inputEmail3" placeholder="Email" required>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input required name="unit" type="text" class="form-control" id="inputEmail3" placeholder="Unidade de Saúde" required>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input required name="unit_address" class="form-control" id="inputEmail3" placeholder="Endereço da Unidade de Saúde" required>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input required name="password" type="password" class="form-control" id="inputPassword3" placeholder="Senha" required>
        </div>
      </div>
    </div>

      <div class="col-sm-12 p-0">
        <button type="submit" class="btn btn-info form-control">Cadastrar</button>
    </div>

  </form>
</div>
@stop
