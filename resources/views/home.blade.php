@extends('adminlte::page')

@section('content_header')
<div class="card-body">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
</div>
@can('is_profissional', App\Post::class)

@section('title', 'Dashboard')
<h1>Administração de Pacientes</h1>

@elsecan('is_admin', App\Post::class)

@section('title', 'Dashboard')
<h1>Painel Administrativo</h1>

@endcan

@stop

@section('content')
<div class="row">
  @can('is_admin', App\Post::class)
  <div class="col-sm-6">
    <div class="card card-widget widget-user-2">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header professional">
        <div class="widget-user-image">
         <span class="glyphicon glyphicon-envelope"></span>
       </div>
       <!-- /.widget-user-image -->
       <h3 class="widget-user-desc">Profissionais da Saúde</h3>
       <h5 class="widget-user-username">Inscritos - 132</h5>
     </div>
   </div>
   <div class="card-footer">
    <div class="row">
      <div class="col-sm-4 border-right">
        <div class="description-block">
          <h5 class="description-header">3,200</h5>
          <span class="description-text">Ativos</span>
        </div>
      </div>
      <div class="col-sm-4 border-right">
        <div class="description-block">
          <h5 class="description-header">13,000</h5>
          <span class="description-text">Presentes</span>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="description-block">
          <h5 class="description-header">35</h5>
          <span class="description-text">Excluídos</span>
        </div>
      </div>
    </div>
    <a href="{{ url('list_professional') }}">
      <div class="row professional text-center">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4 description-block">
          <span>Ver mais</span>
        </div>
        <div class="col-sm-4">
        </div>
      </div>
    </a>
  </div>
</div>
@endcan
@can(['can_see_paciente'], App\Post::class)
<div class="col-sm-6">
  <div class="card card-widget widget-user-2">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header paciente">
      <div class="widget-user-image">
        <span class="glyphicon glyphicon-envelope"></span>
      </div>
      <!-- /.widget-user-image -->
      <h3 class="widget-user-desc">Pacientes</h3>
      <h5 class="widget-user-username">Inscritos - 189</h5>
    </div>
  </div>
  <div class="card-footer">
    <div class="row">
      <div class="col-sm-4 border-right">
        <div class="description-block">
          <h5 class="description-header">3,200</h5>
          <span class="description-text">Ativos</span>
        </div>
      </div>
      <div class="col-sm-4 border-right">
        <div class="description-block">
          <h5 class="description-header">13,000</h5>
          <span class="description-text">Presentes</span>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="description-block">
          <h5 class="description-header">35</h5>
          <span class="description-text">Excluídos</span>
        </div>
      </div>
    </div>

    <a href="{{ url('list_paciente') }}">
      <div class="row paciente">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4 description-block">
          <span>Ver mais</span>
        </div>
        <div class="col-sm-4">
        </div>
      </div>
    </a>

  </div>
</div>
@endcan
@cannot('can_see_paciente')
@cannot('is_paciente')
<span>Você poderá ver os pacientes apos ser ativado pela administração</span>
@endcannot
@endcannot
</div>

<style>
  .professional {
    background-color: #00a0bc;
    color: white;
  }

  .paciente {
    background-color: #00ba44;
    color: white;
  }
</style>
@stop
