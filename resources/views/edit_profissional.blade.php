{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
<div class="card card-info">
<div class="card-header">
   <div class="row">
     <div class="col-sm-12">
       <span class="card-title" style="padding-top: 5px">Editar Profissional</span>
     </div>
   </div>
</div>
<!-- /.card-header -->
<!-- form start -->
<form action="{{ url('profissional_edit_save') }}" method="post" class="form-horizontal">
  @csrf

  <input value="<?= $user->id ?>" name="attribute_id" type="hidden">
  <div class="card-body">
   <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <input value="<?= $user->name ?>" name="name" type="text" class="form-control" id="name" placeholder="Nome" required>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <input value="<?= $user->sobrenome ?>" name="sobrename" type="text" class="form-control" id="name" placeholder="Sobrenome" required>
      </div>
    </div> 
  </div>

  <div class="row">
    <div class="col-sm-8">
      <div class="form-group">
        <select name="especialidade" class="form-control select2" style="width: 100%;">
          <option <?php if ($user->especialidade == 1) echo 'selected="selected"'?> value="1">Médicos</option>
          <option <?php if ($user->especialidade == 2) echo 'selected="selected"'?> value="2">Enfermeiros(as)</option>
          <option <?php if ($user->especialidade == 3) echo 'selected="selected"'?> value="3">Nutricionistas</option>
          <option <?php if ($user->especialidade == 4) echo 'selected="selected"'?> value="4">Farmacêuticos</option>
          <option <?php if ($user->especialidade == 5) echo 'selected="selected"'?> value="5">Psicólogos</option>
          <option <?php if ($user->especialidade == 6) echo 'selected="selected"'?> value="6">Fisioterapeutas</option>
          <option <?php if ($user->especialidade == 7) echo 'selected="selected"'?> value="7">Assistente Social</option>
        </select>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <input required value="<?= $user->crm ?>" name="crm" type="text" class="form-control" id="name" placeholder="CRM" required>
      </div>
    </div> 
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="form-group">
        <input value="<?= $user->email ?>" name="email" type="email" class="form-control" id="inputEmail3" placeholder="Email" required>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="form-group">
        <input value="<?= $user->unit ?>" name="unit" class="form-control" type="text" id="inputEmail3" placeholder="Unidade" required>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="form-group">
        <input value="<?= $user->unit_address ?>" name="unit_address" class="form-control" type="text" id="inputEmail3" placeholder="Endereço da Unidade" required>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <select name="status" class="form-control select2" style="width: 100%;" required>
          <option value="1" <?php if ($user->status == 1) echo 'selected="selected"'?>>Ativado</option>
          <option value="0" <?php if ($user->status == 0) echo 'selected="selected"'?>>Desativado</option>
          </select>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <input name="password" type="text" class="form-control" id="inputPassword3" placeholder="Senha">
      </div>
    </div>
  </div>


    <div class="col-sm-12 p-0">
      <button type="submit" class="btn btn-info form-control">Salvar</button>
    </div>
  
</form>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop