<?php
  $especialidades = \App\Models\Especialidade::all();
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/adminlte.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="{{ url('/') }}"><b>Promalic</b> V.1</a>
  </div>

 <div class="card card-info">
  <div class="card-header">
    <h3 class="card-title">Novo Profissional</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form class="form-horizontal" method="POST" action="{{ route('register') }}">
    @csrf
    <div class="card-body">
     <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <input name="name" type="name" class="form-control" id="name" placeholder="Nome" required>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <input name="sobrenome" type="sobrename" class="form-control" id="name" placeholder="Sobrenome" required>
        </div>
      </div> 
    </div>

    <div class="row">
      <div class="col-sm-8">
        <div class="form-group">
          <select name="especialidade" class="form-control select2" style="width: 100%;" id="especialidade" placeholder="Especialidade" required>
            @foreach($especialidades as $espec)
            <option value="{{$espec['id']}}">{{$espec['nome']}}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <input name="crm" type="CRM" class="form-control" id="name" placeholder="CRM" required>
        </div>
      </div> 
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input name="email" type="email" class="form-control" id="inputEmail3" placeholder="Email" required>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input name="unit" type="unit" class="form-control" id="inputEmail3" placeholder="Unidade de Saúde" required>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input name="unit_address" type="unit_address" class="form-control" id="inputEmail3" placeholder="Endereço da Unidade de Saúde" required>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <input name="password" type="password" class="form-control" id="inputPassword3" placeholder="Senha" required>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <input name="password_confirmation" type="password" class="form-control" id="inputPassword3" placeholder="Repetir Senha" required>
        </div>
      </div>
    </div>

      <div class="col-sm-12 p-0">
        <button type="submit" class="btn btn-info form-control">Cadastrar</button>
    </div>

  </form>
</div>
</div>
</div>


<div>
<p>
<a href="{{url('/')}}"><i class="fas fa-arrow-circle-left"></i>&nbsp;&nbsp;Voltar</a>
</p>
</div>

@if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif

<!-- jQuery -->
<script src="{{ asset('vendor/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('vendor/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
</body>
</html>
