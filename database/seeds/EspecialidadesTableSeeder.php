<?php

use Illuminate\Database\Seeder;

class EspecialidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Especialidade::create(['nome' => 'Médico(a)']);
        App\Models\Especialidade::create(['nome' => 'Enfermeiro(a)']);
        App\Models\Especialidade::create(['nome' => 'Nutricionista']);
        App\Models\Especialidade::create(['nome' => 'Farmacêutico']);
        App\Models\Especialidade::create(['nome' => 'Psicólogo']);
        App\Models\Especialidade::create(['nome' => 'Fisioterapeuta']);
        App\Models\Especialidade::create(['nome' => 'Assistente Social']);
    }
}
