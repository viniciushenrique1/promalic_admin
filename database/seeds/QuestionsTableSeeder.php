<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // ======================================================================================================================

        $question = App\Models\Question::create([
            'question' => 'Conhecimento sobre a doença'
        ]);

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Conhecimento sobre a Insuficiência Cardíaca',
            'especialidades' => '[1],[2]'
        ]);

        App\Models\Item::create([
            'item'           => 'Verificar o conhecimento que eles têm sobre o que é a IC e suas causa.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar de maneira simples o funcionamento normal do coração, o que acontece na IC e indicar a possível causa da IC da pessoa.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        // ======================================================================================================================
        // ======================================================================================================================

        $question = App\Models\Question::create([
            'question' => 'Complicações da Insuficiência Cardíaca – Sinais de Alerta'
        ]);

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Presença de edema',
            'especialidades' => '[1],[2],[3],[6]'
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar sobre a verificação de peso diário no hospital.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Indicar qual peso seco da pessoa e orientá-la sobre o peso que deve ser mantido.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Verificar a disponibilidade de balança na casa da pessoa e, caso seja necessário, indicar um local para que ela possa se pesar.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar sobre a importância de pesar-se diariamente com o mínimo de roupa possível e descalço, sempre no mesmo horário.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Pesar-se diariamente com o mínimo de roupa possível, sempre no mesmo horário.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa procure utilizar sempre a mesma balança, pesando após urinar e antes de comer.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procure utilizar sempre a mesma balança, pesando após urinar e antes de comer.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à importância de anotar o peso todos os dias.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Anotar o peso todos os dias.'
        ]);

        App\Models\Item::create([
            'item'           => 'Ensinar a avaliação do sinal de cacifo e orientar que ele faça essa avaliação diariamente.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa anote a quantidade de cruzes de acordo com os locais do teste de avaliação do edema',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Avaliar o cacifo diariamente conforme orientado na internação e anotar a quantidade de cruzes, de acordo com os locais do teste de avaliação do edema.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar a procurar um profissional e/ou serviço de saúde quando houver aumento de mais de 1,5 kg em um dia.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde quando houver aumento de mais de 1,5kg em um dia.'
        ]);

        // ======================================================================================================================

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Presença de cansaço',
            'especialidades' => '[1],[2],[6]'

        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Identificar qual a tolerância da pessoa à atividade por meio da observação do desenvolvimento das atividades durante a internação.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à importância de observar a ocorrência de cansaço aos grandes, médios e pequenos esforços e/ou ao repouso.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Observar a ocorrência de cansaço aos grandes, médios e pequenos esforços e/ou ao repouso.'
        ]);

        App\Models\Item::create([
            'item'           => 'Indicar em quais situações deve procurar um profissional e/ou serviço de saúde: quando houver cansaço aos pequenos esforços e/ou em repouso.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde quando houver cansaço aos pequenos esforços e/ou repouso.'
        ]);

        // ======================================================================================================================

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Presença de fadiga',
            'especialidades' => '[1],[2],[6]'
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à importância de observar a ocorrência de fadiga aos grandes, médios e pequenos esforços e/ou ao repouso.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Observar a ocorrência de fadiga aos grandes, médios e pequenos esforços e/ou ao repouso.'
        ]);

        App\Models\Item::create([
            'item'           => 'Indicar em quais situações deve procurar um profissional e/ou serviço de saúde: quando houver fadiga aos pequenos esforços e/ou ao repouso.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde quando houver fadiga aos pequenos esforços e/ou ao repouso.'
        ]);

        // ======================================================================================================================

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Presença de dispneia',
            'especialidades' => '[1],[2],[6]'
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à importância de observar a ocorrência de dispneia (falta de ar) aos grandes, médios e pequenos esforços e/ou ao repouso.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Observar a ocorrência de falta de ar aos grandes, médios e pequenos esforços e/ou ao repouso.'
        ]);
        
        App\Models\Item::create([
            'item'           => 'Indicar em quais situações deve procurar um profissional e/ou serviço de saúde: quando a pessoa acordar de repente à noite, necessitando se sentar para recuperar o fôlego; quando houver necessidade de usar mais de um travesseiro para dormir;',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde'
        ]);

         // ======================================================================================================================

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Presença de taquicardia',
            'especialidades' => '[1],[2]'
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id'],
        ]);

        App\Models\Item::create([
            'item'           => 'Ensinar a avaliar os batimentos cardíacos.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
        ]);

        App\Models\Item::create([
            'item'           => 'Identificar e orientar qual o valor médio de batimentos cardíacos da pessoa.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id'],
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à importância de observar a ocorrência de alterações nos batimentos cardíacos
            (muito lento ou muito rápido ou alternando muito entre eles) e considerar o valor de 75 batimentos
            por minuto para comparação.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Observar a ocorrência de batimentos cardíacos muito lentos ou muito rápidos ou
            alternando entre eles, considerando o valor de 75 batimentos por minuto para comparação.'
        ]);

        App\Models\Item::create([
            'item'           => 'Indicar em quais situações deve procurar um profissional e/ou serviço de saúde: quando os
            batimentos cardíacos estiverem muito acima ou abaixo do valor médio da pessoa',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde quando os batimentos cardíacos
            estiverem muito acima ou abaixo do valor médio identificado na internação.'
        ]);

        // ======================================================================================================================

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Identificação do nível de resposta verbal',
            'especialidades' => '[1],[2]'
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id'],
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à ocorrência de mudança brusca no contato verbal ou na formulação de frases,
            tontura, dificuldade de concentração e desmaio',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa deve procurar um profissional e/ou serviço de saúde caso perceba alguma dessas alterações',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde caso ocorra mudança brusca nas
            conversas, tontura, dificuldade de concentração e desmaio.'
        ]);

        // ======================================================================================================================
        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Identificação de alteração no padrão urinário',
            'especialidades' => '[1],[2]'
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id'],
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa deve estar atenta quanto à diminuição do volume de urina nas 24 horas, de
            acordo com a rotina pessoal, e também na cor e no cheiro da urina',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa fique atenta à necessidade de urinar à noite.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa procure um profissional e/ou serviço de saúde caso esse sintoma esteja presente.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde caso ocorra diminuição da
            quantidade de urina nas 24 horas e/ou mudança na cor/cheiro da urina e/ou necessidade de urinar à noite.'
        ]);

        // ======================================================================================================================
        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Identificação de constipação',
            'especialidades' => '[1],[2],[3]'
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id'],
        ]);

        App\Models\Item::create([
            'item'           => 'Despertar a pessoa para observar os hábitos intestinais.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à presença de desconforto abdominal e/ou ausência de eliminação de gases.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa procure um profissional e/ou serviço de saúde caso esse sintoma estejapresente.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde caso ocorra desconforto na
            barriga e/ou ausência de eliminação de gases.'
        ]);

        // ======================================================================================================================
        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Identificação de plenitude gástrica',
            'especialidades' => '[1],[2],[3]'
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Atentar quanto à sensação de plenitude gástrica, dor no estômago ou sensação de endurecimento, perda de apetite ou náuseas.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);
        
        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa procure um profissional e/ou serviço de saúde caso esse sintoma esteja presente',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde caso ocorra sensação de estômago cheio e/ou dor no estômago e/ou sensação de barriga dura e/ou perda de apetite e/ou náuseas.'
        ]);

        // ======================================================================================================================
        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Presença de tosse',
            'especialidades' => '[1],[2],[6]'
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à importância de observar a presença de tosse seca, e/ou peito cheio e/ou saliva espumosa rosa.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Ficar atento à presença de tosse seca e/ou peito cheio e/ou saliva espumosa rosa.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa identifique o horário em que há piora da tosse.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Identificar o horário em que ocorre piora da tosse.'
        ]);

        App\Models\Item::create([
            'item'           => 'Indicar em quais situações deve procurar um profissional e/ou serviço de saúde: quando a tosse for pior no período noturno.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde quando a tosse for pior no período noturno.'
        ]);

        // ======================================================================================================================
        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Identificação dos sons pulmonares',
            'especialidades' => '[1],[2],[6]'
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa e os familiares fiquem atentos aos sons pulmonares que podem ser emitidos ao respirar.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que o som pulmonar pode se assemelhar ao chiado.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa procure um profissional e/ou serviço de saúde caso esse sintoma esteja presente.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde quando ouvir um som parecido
            com um chiado ao respirar.'
        ]);

        // ======================================================================================================================
        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Presença de cianose',
            'especialidades' => '[1],[2],[6]'
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à ocorrência de cianose (ou cor arroxeada) das pontas dos dedos..',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa procure um profissional e/ou serviço de saúde caso esse sintoma esteja presente.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde quando observar a cor arroxeada nas pontas dos dedos.'
        ]);

        // ======================================================================================================================
        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Presença de ascite',
            'especialidades' => '[1],[2]'
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => ' Orientar quanto à ocorrência de edema ou aumento do abdome.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa procure um profissional e/ou serviço de saúde caso esse sintoma esteja presente.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde caso ocorra inchaço ou aumento na barriga.'
        ]);

         // ======================================================================================================================
         $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Identificação da pressão venosa jugular elevada',
            'especialidades' => '[1],[2]'
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar à pessoa e aos familiares por que esse sintoma acontece.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar aos familiares para que fiquem atentos quanto à ocorrência de dilatação nas veias do pescoço.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa procure um profissional e/ou serviço de saúde caso esse sintoma esteja presente.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar um profissional e/ou serviço de saúde caso ocorra aumento nas veias do pescoço.'
        ]);

        // ======================================================================================================================
        
        $question = App\Models\Question::create([
            'question' => 'Tratamento Farmacológico'
        ]);

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto aos medicamentos e sua utilização (Médicos, Enfermeiros e Farmacêuticos)',
            'especialidades' => '[1],[2]'
        ]);
        
        App\Models\Item::create([
            'item'           => 'Certificar de que a pessoa e familiares entenderam o que está escrito na receita médica.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto aos nomes dos medicamentos, indicação, efeitos terapêuticos, tomada e instruções especiais.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => ' Orientar quanto à via correta de administração de cada medicamento prescrito e como deve ser tomado.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar para que a pessoa e familiares estejam atentos aos horários corretos das medicações;
            orientar que, se necessário, seja utilizado despertador ou celular para lembrar os horários dos medicamentos.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Ficar atento aos horários corretos das medicações. Se necessário, utilizar despertador ou celular para lembrar os horários.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar às pessoas com IC e familiares para que procedam à identificação dos horários em que os medicamentos devem ser tomados, nos casos de analfabetismo.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => '(Identificar, caso seja necessário, os horários em que os medicamentos devem ser tomados conforme o período (manhã, tarde ou noite) por meio de desenhos de sol, lua etc.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que, caso seja necessário, pode-se separar os medicamentos nas quantidades exatas de cada horário.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Separar os medicamentos nas quantidades certas para cada horário, em caso de necessidade.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que sejam listadas e organizadas as medicações de uso diário, incluindo todas as prescrições (receitas), medicamentos de venda livre, vitaminas e suplementos.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Listar e organizar os medicamentos de uso diário, inclusive aqueles de outras receitas, as vitaminas/suplementos e os medicamentos de venda livre'
        ]);
        
        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa ou familiares observem, rotineiramente, a quantidade das medicações que
            tem em casa, de forma que seja abastecido em tempo hábil e esteja seguro se precisar esperar alguns
            dias para obter mais remédios em farmácia pública ou privada.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Ficar atento à quantidade das medicações que tem em casa, de forma a estar seguro caso precise esperar alguns dias para obter mais remédios.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à continuidade dos medicamentos anteriores, se recomendado.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que o tratamento seja mantido, mesmo que a pessoa esteja se sentindo melhor e/ou não tenha sintomas.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Não parar o tratamento, mesmo que esteja se sentindo melhor e/ou não tenha sintomas.'
        ]);

        // ======================================================================================================================

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto aos efeitos colaterais dos medicamentos',
            'especialidades' => '[1],[4]'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar para que sejam monitorados os seguintes efeitos colaterais dos medicamentos:',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Presença de cãibras e sua frequência, que pode ocorrer devido ao baixo teor de potássio'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa com IC e familiares questionem ao médico ou ao enfermeiro do serviço de
            saúde sobre o nível de potássio encontrado em exames de sangue e entender se o nível está
            adequado ou se é necessário limitar ou repor esse mineral.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Questionar ao médico ou ao enfermeiro do serviço de saúde sobre o nível de potássio no sangue e entender se está adequado ou se é preciso limitar ou repor.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa com IC e familiares questionem ao médico ou ao enfermeiro do serviço de
            saúde sobre a função renal e entender o que deve ser feito para solucionar, em caso de
            comprometimento',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Questionar ao médico ou ao enfermeiro do serviço de saúde sobre os exames dos rins e entender o que deve ser feito para resolver, caso esteja com alterações.'
        ]);

         // ======================================================================================================================

         $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto à reconciliação medicamentosa',
            'especialidades' => '[1],[2],[4],[3]'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto a existência de um serviço de anticoagulação, caso faça uso de varfarina
            (Marevan®) ou outro anticoagulante; será necessário realizar exames de sangue com certa
            frequência para que o profissional possa se certificar da dosagem correta do medicamento.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa evite a auto-medicação; não tomar outros medicamentos ou ervas sem questionar ao médico.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Não tomar medicamentos ou ervas sem falar com o médico.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa não faça uso de sildenafil, vardenafil, tadalafil ou qualquer erva para manter
            a atividade sexual sem antes conversar com o médico.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => ' Orientar que seja observada com atenção a presença de sinais/sintomas, procurando o serviço de
            saúde conforme orientado anteriormente e que os medicamentos sejam tomados conforme a receita
            médica.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Tomar os medicamentos conforme a receita médica e procurar o serviço de saúde caso seja observada a presença de algum sinal/sintoma.'
        ]);

        // ======================================================================================================================
        
        $question = App\Models\Question::create([
            'question' => 'Tratamento Não-Farmacológico'
        ]);

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto à ingestão de líquidos',
            'especialidades' => '[1],[2],[4],[3]'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar para que seja obedecida a quantidade de líquidos a ser ingerida nas 24 horas (de acordo com o critério médico',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que alguns alimentos são considerados líquidos, incluindo pudim, gelatina, sopas (grossas ou finas), picolés e sorvete.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Ficar atento quanto à ingestão de alguns alimentos que são considerados líquidos, como pudins, gelatinas, sopas, picolés e sorvetes.)'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que seja feito o manejo do excesso de líquido, caso perceba que houve aumento no peso e/ou sinta falta de ar, reduza a quantidade de sódio ou de líquidos por dois dias.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Reduzir a quantidade de sódio ou de líquidos por dois dias caso perceba que houve aumento no peso e/ou sinta falta de ar'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à utilização de copo com marcação em “mL” para controlar a quantidade de líquidos ingeridos, caso seja necessário',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Utilizar copo com marcação em ‘mL’ para controlar a quantidade de líquidos.'
        ]);

        // ======================================================================================================================

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto à alimentação',
            'especialidades' => '[1],[2],[4],[3]'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que deve ser seguida a dieta, conforme plano nutricional determinado para as suas condições clínicas – plano nutricional em anexo',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Explicar o que ocorre quando o sódio está em excesso no corpo.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto ao limite de consumo de sódio e à importância de fazer as adequações necessárias na dieta',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que é importante ler rótulos dos alimentos, com atenção à dieta',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Ler rótulo dos alimentos, com atenção à quantidade de sódio.'
        ]);

        App\Models\Item::create([
            'item'           => ' Orientar para que sejam evitados alimentos industrializados, “comidas rápidas”, molhos e outros
            alimentos que contém muito sódio, como sopas prontas, enlatados, salgadinhos, embutidos, pizzas,
            hambúrgueres, macarrão instantâneo, molho de soja, picles etc.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Evitar alimentos industrializados, ‘comidas rápidas, molhos, sopas prontas, enlatados, salgadinhos, embutidos, pizzas, hambúrgueres, macarrão instantâneo, molho de soja, picles etc.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar para que sejam utilizados temperos naturais para o preparo das refeições, abandonando o consumo de temperos prontos, que contém muito sódio.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar para que, caso possível, seja realizado o plantio dos temperos de preferência em horta caseira para facilitar o consumo',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Utilizar temperos naturais para o preparo das refeições. Caso seja possível, realizar o plantio dos temperos de preferência em horta caseira para facilitar o consumo.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar para que não seja adicionado sal na comida após o preparo e que seja retirado o saleiro da mesa',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Retirar o saleiro da mesa, pois o sal não deve ser adicionado à comida após seu preparo.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar aos que necessitarem de acompanhamento nutricional sobre a importância de manter contato frequente com nutricionista.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar para que a pessoa se alimente com pequenas porções em cada horário, várias vezes
            durante o dia, pois pode ajudar na digestão e evitar a plenitude gástrica após a refeição',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Alimentar-se com pequenas porções em cada horário, várias vezes durante o dia.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar para que a pessoa evite permanecer muito tempo sem se alimentar, para que não exagere na próxima refeição.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Evitar permanecer muito tempo sem se alimentar.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a refeição da noite (jantar) aconteça algum tempo antes de deitar, para que não haja incômodo no período da noite.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Jantar algum tempo antes de deitar.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar para que a pessoa diminua o consumo de cafeína.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Diminuir o consumo de cafeína.'
        ]);

        // ======================================================================================================================

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto ao abandono do tabagismo e redução do consumo de bebidas alcoólicas',
            'especialidades' => '[1],[2],[4],[3],[5],[6]'
        ]);
       
        App\Models\Item::create([
            'item'           => 'Orientar sobre a importância de abandonar o hábito de fumar.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Evitar ficar próximo de pessoas que estiverem fumando e solicitar que as pessoas não fumem de casa.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa evite o “fumo passivo”, ou seja, evite ficar próximo de pessoas que
            estiverem fumando e solicite que as pessoas não fumem em casa.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Evitar ficar próximo de pessoas que estiverem fumando e solicitar que as pessoas não fumem de casa.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que, caso seja necessário, a pessoa pode solicitar auxílio para parar de fumar.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Dar andamento na solicitação do auxílio para parar de fumar.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar sobre a importância de abandonar o hábito de consumir bebidas alcoólicas, pois qualquer
            quantidade de álcool pode levar a descompensação do coração e comprometer a ação dos medicamentos.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Evitar ficar próximo de pessoas que estiverem fumando e solicitar que as pessoas não fumem de casa.'
        ]);

        // ======================================================================================================================

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto aos cuidados para prevenção de outros agravos',
            'especialidades' => '[1],[2],[4],[3],[5]'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto a necessidade de buscar o serviço de saúde para verificar a carteira de vacinação e
            vacinar contra pneumonia e gripe, além de outras conforme a necessidade.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à procura do serviço de saúde para avaliação e acompanhamento da saúde bucal.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar serviço de saúde para avaliação e acompanhamento da saúde bucal, conforme encaminhamento.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à importância de controlar a pressão arterial em casa ou no serviço de saúde.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Controlar a pressão arterial em casa ou no serviço de saúde.'
        ]);

        App\Models\Item::create([
            'item'           => 'Ensinar a automedida da pressão arterial.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à importância de controlar a glicemia em casa ou no serviço de saúde.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Controlar a glicemia em casa ou no serviço de saúde.'
        ]);

        // ======================================================================================================================
        
        $question = App\Models\Question::create([
            'question' => 'Exercícios/Atividades Físicas'
        ]);

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto à realização de exercícios / atividades físicas',
            'especialidades' => '[1],[2],[6]'
        ]);

        App\Models\Item::create([
            'item'           => ' Orientar quanto aos exercícios respiratórios, incentivando para que possa agir com disciplina e motivação.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa participe de caminhadas e outros exercícios físicos, sob supervisão de um profissional.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa converse sempre com o médico ou fisioterapeuta sobre a rotina de
            exercícios, pois, em alguns casos, pode ser necessário evitar certos exercícios ou ter outras restrições.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Conversar sempre com o médico ou fisioterapeuta sobre a rotina de exercícios.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar, quando necessário, que a pessoa busque o(s) centro(s) de reabilitação cardíaca para triagem, avaliação e possível acompanhamento.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar o(s) centro(s) de reabilitação cardíaca para triagem, avaliação e possível acompanhamento, conforme encaminhamento.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa procure academias ao ar livre e centros comunitários, se já estiver liberado para a realização de atividades e caso seja mais viável financeiramente.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar academias ao ar livre e centros comunitários.'
        ]);

         // ======================================================================================================================
        
         $question = App\Models\Question::create([
            'question' => 'Atividades de Vida Diária'
        ]);

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto à realização de atividades',
            'especialidades' => '[1],[2],[5],[6],[7]'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa retorne aos poucos às tarefas corriqueiras e que deve tentar realizá-las sozinho(a) e só pedir ajuda caso não seja possível.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Retornar aos poucos às tarefas corriqueiras, tentando realizá-las sozinho. Pedir ajuda somente caso não seja possível.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa evite atividades que produzam cansaço.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Evitar atividades que produzam cansaço.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa faça pausas antes, durante e depois das atividades ou mude a maneira de
            fazê-las, reduzindo-a ou dividindo-as em pequenas tarefas.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Fazer pausas antes, durante e depois das atividades. Ou então: reduzir as atividades ou dividi-las em pequenas tarefas.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à importância de dormir o suficiente no período da noite.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Dormir o suficiente no período da noite.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa tome atitudes para reduzir o estresse, definindo limites para si mesmo.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa organize as atividades de vida diária, de forma a permitir períodos de soneca e repouso',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Organizar as atividades de vida diária, de forma a permitir períodos de soneca e repouso.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar sobre a importância de descansar o suficiente para colocar também o coração em repouso.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar sobre a importância de descansar o suficiente para colocar também o coração em repouso.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à necessidade de adaptação em casa para facilitar a movimentação, caso seja possível.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa evite subir escadas de forma contínua, fazendo pausas conforme a necessidade de descanso.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Orientar que a pessoa evite subir escadas de forma contínua, fazendo pausas conforme a necessidade de descanso.'
        ]);

         // ======================================================================================================================

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações em casos de viagens',
            'especialidades' => '[1],[2],[5],[6],[7]'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa questione sempre o médico sobre planos de viagem e certifique-se da
            quantidade de medicamentos necessária para a duração da viagem ou da possibilidade de adquirir os mesmos no período em que estiver fora da cidade.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Questionar sempre o médico sobre planos de viagem, certificando-se da
            quantidade de medicamentos necessária para a duração da viagem ou da possibilidade de adquirir os
            mesmos no período em que estiver fora da cidade.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar a pessoa para embalar os medicamentos na bagagem de mão, em casos de viagens de
            avião; pode ser necessária uma carta do médico sobre os medicamentos, principalmente em viagens internacionais.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Embalar os medicamentos na bagagem de mão , em casos de viagens de avião.'
        ]);

        // ======================================================================================================================
        
        $question = App\Models\Question::create([
            'question' => 'Sexualidade'
        ]);

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto à sexualidade',
            'especialidades' => '[1],[2],[5]'
        ]);

        App\Models\Item::create([
            'item'           => ' Investigar se a atividade sexual se constitui em uma preocupação.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto ao retorno à vida sexual, conforme as condições clínicas.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à importância de conversar com o(a) parceiro(a) a respeito da sexualidade.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Conversar com o(a) parceiro”a) a respeito da sexualidade.'
        ]);

        // ======================================================================================================================
        
        $question = App\Models\Question::create([
            'question' => 'Recursos Existentes'
        ]);

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto ao processo de referência-contra-referência',
            'especialidades' => '[1],[2],[3],[4],[5],[6],[7]'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à importância de dar continuidade nos encaminhamentos solicitados na alta; caso seja necessário, procurar o serviço de saúde próximo à residência para esclarecimentos.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => ' Orientar quanto às consultas agendadas e a importância de seu comparecimento, cuja data será agendada de acordo com a Classificação Funcional da New York Heart Association (NYHA), a
            saber: as pessoas que foram internadas com classe funcional III e IV devem ser agendadas para retorno em até 14 dias com o cardiologista no hospital e aquelas com classe I e II devem ser
            encaminhadas para o clínico geral da rede; caso não haja previsão de data para consulta com o clínico geral da rede, poderá ser agendada consulta com o clínico geral ou com o cardiologista do hospital em até 30 dias.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa esteja sempre munida com cartão de identificação com dados relacionados à doença, medicações em uso e contato telefônico.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);
        
        App\Models\Item::create([
            'item'           => 'Referenciar a pessoa com IC e familiares ao serviço e profissional de saúde para o acompanhamento.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        // ======================================================================================================================
        
        $question = App\Models\Question::create([
            'question' => 'Cuidados Paliativos'
        ]);

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto aos cuidados paliativos',
            'especialidades' => '[1],[2],[3],[4],[5],[6],[7]'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa tem o direito de participar das tomadas de decisões quanto aos cuidados,
            com demonstração de suas percepções acerca do tratamento e dos ajustes necessários para adaptarse a ele.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Você tem o direito de participar das tomadas de decisões quanto aos cuidados, demonstrando suas percepções sobre o tratamento e os ajustes necessários para adaptar-se a ele.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa pode disponibilizar um documento legal, denominado Diretivas Antecipadas, para que familiares, amigos e profissionais de saúde saibam dos seus desejos em
            relação aos cuidados de saúde',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Você pode disponibilizar um documento legal, denominado Diretivas Antecipadas, para que familiares, amigos e profissionais de saúde saibam dos seus desejos em relação aos cuidados de saúde'
        ]);

        App\Models\Item::create([
            'item'           => ' Conversar com os familiares, a fim de que todos estejam cientes das preferências de atendimento médico da pessoa',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Referenciar a pessoa com IC e familiares ao serviço e profissional de saúde para acompanhamento.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        // ======================================================================================================================
        
        $question = App\Models\Question::create([
            'question' => 'Direitos da pessoa com IC e Familiares'
        ]);

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto aos direitos de compreender sobre a doença',
            'especialidades' => '[1],[2],[3],[4],[5],[6],[7]'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa tem o direito de saber sobre o prognóstico; conforme cada caso, saber informações sobre cuidados paliativos.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Você tem o direito de saber como fica sua vida após essa internação.'
        ]);

        App\Models\Item::create([
            'item'           => 'Verificar o entendimento do paciente e familiar/cuidador e reforçar as orientações sobre o autocuidado, conforme a necessidade',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa tem o direito de ter um cuidador/familiar que o acompanhe em suas atividades e que esteja envolvido no plano de tratamento.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Você tem o direito de ter um cuidador/familiar que o acompanhe em suas atividades e que esteja envolvido no plano de tratamento.'
        ]);

        App\Models\Item::create([
            'item'           => 'Entregar material por escrito com as prescrições e orientações após a alta hospitalar.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Certificar-se de que a pessoa e os familiares tenham entendido o conteúdo e, caso seja necessário, fazer a leitura do material em conjunto, esclarecendo as questões que ficarem em dúvida.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa e familiares utilizem o material recebido para leituras em casa.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Utilizar o material recebido para leituras em casa.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa e familiares procurem o serviço de saúde para esclarecimentos, caso haja dúvidas',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar o serviço de saúde para esclarecimentos, caso haja dúvidas.'
        ]);

        // ======================================================================================================================

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto ao acesso gratuito às medicações',
            'especialidades' => '[1],[2],[4],[7]'
        ]);

        App\Models\Item::create([
            'item'           => ' Orientar quanto aos medicamentos prescritos que são disponibilizados pelo governo mediante
            processo assinado pelo médico; explicar o que deve ser feito após a alta, com as orientações para a aquisição do(s) medicamento(s).',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à procura do serviço de saúde próximo à residência para adquirir os medicamentos que são disponibilizados pelo governo.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar o serviço de saúde próximo à residência para adquirir os medicamentos que são disponibilizados pelo governo.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto ao acesso às farmácias convencionais, questionando se as mesmas são cadastradas
            na chamada “Farmácia Popular”; orientar que, em alguns locais, é necessária documentação para a aquisição de medicamentos com baixo custo.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar as farmácias com cadastro na “Farmácia Popular’, a fim de adquirir medicamentos gratuitamente ou com baixo custo.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à procura do serviço de saúde próximo à residência para garantia do acesso à receita e à renovação da mesma.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Aparecer para o paciente: “Procurar o serviço de saúde próximo à residência se
            houver dúvidas quanto ao acesso aos medicamentos e também para garantir o acesso à receita e à renovação da mesma.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa e familiares procurem o serviço de saúde próximo à residência caso haja alguma dúvida quanto ao acesso aos medicamentos.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id']
        ]);

        // ======================================================================================================================

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto ao retorno à vida profissional',
            'especialidades' => '[1],[2],[5],[6],[7]'
        ]);

        App\Models\Item::create([
            'item'           => 'Conversar sobre o retorno à vida profissional da pessoa com IC, com discussão sobre o tipo de trabalho que executa e sobre os possíveis efeitos no problema de coração; explicar que pode ser
            necessário fazer adaptações para o retorno ao trabalho.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        // ======================================================================================================================

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações quanto aos benefícios concedidos pelo governo',
            'especialidades' => '[1],[2],[5],[7]'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à procura do Instituto Nacional de Seguridade Social (INSS) para proceder às
            questões previdenciárias, incluindo a isenção do imposto de renda na aposentadoria, reforma ou
            pensão, pelo número 135 (Previdência Social); em condições de limitação e dificuldade da pessoa e/ou familiares, realizar o contato direto com a Previdência Social.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à procura da instituição responsável pelo serviço de transportes da cidade para
            inclusão do paciente e/ou cuidador em programa sobre gratuidade do transporte ao portador de
            doença crônica, como vale-social, passe-livre e Tratamento Fora do Domicílio-TFD.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à procura de instituições responsáveis por auxílios financeiros, como o Benefício de Prestação Continuada-BPC.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à procura da Defensoria Pública para resolver questões relacionadas à curatela
            e/ou procuração ou ao óbito do paciente, no que se refere ao registro em cartório, gratuidade do sepultamento e pensão por morte.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à procura do Serviço Social de referência, em casos de internação, para solicitar
            autorização para permanência do familiar.',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à procura da Defensoria Pública para garantir o direito de presença de acompanhante (legislação ou rotina institucional).',
            'type'           => 'textarea',
            'subquestion_id' => $subquestion['id']
        ]);
        
        // ======================================================================================================================
        
        $question = App\Models\Question::create([
            'question' => 'Empoderamento para Autonomia e Autocuidado'
        ]);

        $subquestion = App\Models\SubQuestion::create([
            'question_id'    => $question['id'],
            'subquestion'    => 'Informações para autonomia e autocuidado',
            'especialidades' => '[1],[2],[3],[4],[5],[6],[7]'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar que a pessoa deve se estimular diariamente para a realização de atividades, seja em casa,
            na rua, no centro comunitário etc.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Estimular-se diariamente para a realização de atividades, seja em casa, na rua, no centro comunitário etc.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à importância de motivar-se para a procura dos serviços orientados na alta, seja para consultas e exames ou para a aquisição de medicamentos etc.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Motivar-se para a procura dos serviços orientados na alta, seja para consultas e exames ou para a aquisição de medicamentos etc.'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar quanto à importância de perceber que, quando o tratamento é seguido corretamente, pode-se viver com mais qualidade de vida.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Perceber que, quando o tratamento é seguido corretamente, pode-se viver com mais qualidade de vida'
        ]);

        App\Models\Item::create([
            'item'           => 'Orientar a procurar o Serviço Social do hospital durante a semana, caso a alta hospitalar aconteça em finais de semana.',
            'type'           => 'checkbox',
            'subquestion_id' => $subquestion['id'],
            'observation'    => 'Procurar o Serviço Social do hospital durante a semana.'
        ]);
    }
}
