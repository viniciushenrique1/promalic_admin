<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersAdminSeeder::class);

        $this->call(QuestionsTableSeeder::class);
        $this->call(EspecialidadesTableSeeder::class);
        Artisan::call('passport:install', ['--force' => '']);
    }
}
