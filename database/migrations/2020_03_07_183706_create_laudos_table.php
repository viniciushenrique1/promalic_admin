<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaudosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laudos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user');
            $table->string('checkbox')->nullable();
            $table->string('pergunta_1')->nullable();
            $table->string('pergunta_2')->nullable();
            $table->string('pergunta_3')->nullable();
            $table->string('pergunta_4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laudos');
    }
}
