<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_pacientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cpf');
            $table->string('sobrenome');
            $table->bigInteger('user');
            $table->string('nascimento');
            $table->string('endereco');
            $table->integer('possui_doenca');
            $table->string('doenca')->nullable();
            $table->integer('com_quem_mora');
            $table->string('com_quem_mora_outro')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_pacientes');
    }
}
