<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributeProfissionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_profissionals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sobrenome');
            $table->bigInteger('user');
            $table->integer('especialidade');
            $table->string('crm');
            $table->string('unit');
            $table->string('unit_address');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_profissionals');
    }
}
