<?php

// use Illuminate\Http\Request;

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'AuthController@login');

Route::middleware('auth:api')->group(function () {
    Route::delete('logout', 'AuthController@logout');

    Route::get('reports', 'ReportController@listApi');
});
