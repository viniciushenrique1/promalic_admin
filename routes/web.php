<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/register', function () {
    return view('auth.register');
});



Route::get('new_professional', 'ProfissionalController@new')->middleware('can:is_admin, post');

Route::get('list_professional', 'ProfissionalController@list')->middleware('can:is_admin, post');

Route::post('profissional', 'ProfissionalController@save')->middleware('can:is_admin, post');

Route::get('edit_profissional/{id}', 'ProfissionalController@edit')->middleware('can:is_admin, post');

Route::post('profissional_edit_save', 'ProfissionalController@save_edit')->middleware('can:is_admin, post');

Route::get('profissional_deletar/{user_id}/{attributes_id}', 'ProfissionalController@delete')->middleware('can:is_admin, post');





Route::get('/new_paciente', function () {
    return view('new_paciente');
})->middleware('can:can_see_paciente, post');

Route::get('/list_paciente', 'PacienteController@list')->middleware('can:can_see_paciente, post');

Route::get('/edit_paciente/{id}', 'PacienteController@edit')->middleware('can:can_see_paciente, post');

Route::post('/pacientes_edit_save', 'PacienteController@save_edit')->middleware('can:can_see_paciente, post');

Route::post('/pacientes', 'PacienteController@save')->middleware('can:can_see_paciente, post');
Route::get('/pacientes_deletar/{user_id}/{attributes_id}', 'PacienteController@delete')->middleware('can:can_see_paciente, post');





Route::get('/new_laudo/{user}', function () {
    return view('new_laudo');
})->middleware('can:can_see_paciente, post');

Route::post('/laudos', 'LaudoController@save')->middleware('can:can_see_paciente, post');

Route::get('/list_laudos', 'LaudoController@list')->middleware('can:is_paciente, post');





Route::get('report-check/{user_id}', 'ReportController@check')->middleware('can:can_see_paciente, post');
Route::get('report-view/{user_id}', 'ReportController@view');
Route::post('report-check-list', 'ReportController@checkList')->middleware('can:can_see_paciente, post');
Route::post('report-check-list-save', 'ReportController@checkListSave')->middleware('can:can_see_paciente, post');
Route::get('report-list', 'ReportController@list')->middleware('can:is_paciente, post');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::post('/api/login', 'Api\LoginController@index');
// Route::get('/api/profissionais', 'Api\ProfissionaisController@list');
// Route::post('/api/profissionais', 'Api\ProfissionaisController@save');

// Route::get('/api/pacientes', 'Api\PacientesController@list');
// Route::post('/api/pacientes', 'Api\PacientesController@save');
// Route::get('/api/laudo', 'Api\PacientesController@laudo');