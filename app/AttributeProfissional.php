<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeProfissional extends Model
{
    protected $fillable = [
        'sobrenome', 'user', 'especialidade', 'crm', 'unit', 'unit_address', 'status'
    ];

    public function espec()
    {
        return $this->hasOne('App\Models\Especialidade', 'id', 'especialidade');
    }
}
