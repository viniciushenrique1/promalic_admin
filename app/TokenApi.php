<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TokenApi extends Model
{
    protected $fillable = [
        'user', 'token'
    ];
}
