<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\AttributeProfissional;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('is_admin', function ($user) {
            return $user->user_type == 1;
        });

        Gate::define('is_profissional', function ($user) {
            return $user->user_type == 2;
        });

        Gate::define('is_paciente', function ($user) {
            return $user->user_type == 3;
        });

        Gate::define('can_see_paciente', function ($user) {
            $userAttributes = AttributeProfissional::where('user', $user->id)->first();

            return ($user->user_type == 1) || ($user->user_type == 2 && $userAttributes->status);
        });

        Passport::routes();
    }
}
