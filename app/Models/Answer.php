<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'report_id',
        'item_id',
        'checkbox',
        'textarea'
    ];

    protected $table = 'answers';

    public function item()
    {
        return $this->hasOne('App\Models\Item', 'id', 'item_id');
    }
}
