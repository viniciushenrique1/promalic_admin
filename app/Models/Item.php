<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'subquestion_id',
        'item',
        'type',
        'observation',
    ];

    protected $table = 'items';
}
