<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Report extends Model
{
    protected $fillable = [
        'user_id',
        'paciente_id',
    ];

    protected $table = 'reports';

    protected $appends = ['created_at_ptbr'];

    public function getCreatedAtPtbrAttribute()
    {
        return Carbon::parse($this->created_at)->format('d/m/Y H:i:s');
    }

    public function answers()
    {
        return $this->hasMany('App\Models\Answer', 'report_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
