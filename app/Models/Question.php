<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question',
    ];

    protected $table = 'questions';

    public function subquestions()
    {
        return $this->hasMany('App\Models\SubQuestion', 'question_id', 'id');
    }
}
