<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubQuestion extends Model
{
    protected $fillable = [
        'question_id',
        'subquestion',
        'especialidades'
    ];

    protected $table = 'subquestions';

    public function items()
    {
        return $this->hasMany('App\Models\Item', 'subquestion_id', 'id');
    }
}
