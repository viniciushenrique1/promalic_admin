<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laudo extends Model
{
    protected $fillable = [
        'user', 'checkbox', 'pergunta_1', 'pergunta_2', 'pergunta_3', 'pergunta_4'
    ];
}
