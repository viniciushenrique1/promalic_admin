<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Laudo;
use View;
use Auth;

class LaudoController extends Controller
{
    public function save(Request $request)
    {
        Laudo::create([
        	'user' => $request->paciente_id,
            'checkbox' => $request->checkbox_1,
            'pergunta_1' => $request->pergunta_1,
            'pergunta_2' => $request->pergunta_2,
            'pergunta_3' => $request->pergunta_3,
            'pergunta_4' => $request->pergunta_4
        ]);

        return redirect()->action('PacienteController@list');
    }

    public function list()
    {
        $userId = Auth::user()->id;

    	$responseData = [];
            
        $laudos = Laudo::where('user', $userId)->get();

        foreach ($laudos as $laudo) {
            // $date = date_format($laudo->created_at,"d/m/Y");
            $date = $laudo->created_at;
            if (isset($responseData[$date])) {
                $responseData[$laudo->created_at][] = $laudo;
            } else {
                $responseData[$date] = [$laudo];
            }
        }

        return View::make('list_laudos')->with('laudos', $responseData);
    }
}
