<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Report;
use App\Models\Answer;
use App\Models\Item;
use App\AttributeProfissional;
use Exception;
use Illuminate\Support\Facades\DB;
use Log;
use Auth;

class ReportController extends Controller
{
    public function check($id)
    {
        if (Auth::user()->user_type == 1) {
            $questions = Question::with('subquestions')->get();
        } elseif (Auth::user()->user_type == 2) {
            $prof = AttributeProfissional::where('user', Auth::user()->id)->first();

            $espec = '[' . $prof['especialidade'] . ']';

            $questions = Question::with(['subquestions' => function ($q) use ($espec) {
                $q->where('subquestions.especialidades', 'like', '%' . $espec . '%');
            }])
            ->get();
        }
        
        $paciente_id = $id;

        return view('reports.check', compact('questions', 'paciente_id'));
    }

    public function checkList(Request $request)
    {
        $input = $request->all();

        if (isset($input['subquestions'])) {
            if (sizeof($input['subquestions']) > 0) {
                $questions = Question::with(['subquestions' => function ($q) use ($input) {
                    $q->whereIn('subquestions.id', $input['subquestions']);
                }, 'subquestions.items'])
                ->get();
            }
        }

        if (empty($questions)) {
            return redirect()->back()->withError('Nenhum item selecionado!');
        }

        $paciente_id = $input['paciente_id'];

        return view('reports.checkList', compact('questions', 'paciente_id'));
    }

    public function checkListSave(Request $request)
    {
        $input = $request->all();

        DB::beginTransaction();
        try {
            $report = Report::create([
                'user_id'     => Auth::user()->id,
                'paciente_id' => $input['paciente_id']
            ]);
        } catch (Exception $e) {
            Log::error($e);
            DB::rollBack();
            return redirect('list_paciente');
        }

        foreach ($input['items'] as $key => $value) {
            $item = Item::find($key);

            $answer = [
                'report_id' => $report['id'],
                'item_id'   => $key
            ];

            if ($item['type'] == 'checkbox') {
                $answer = array_merge($answer, ['checkbox' => $value]);
            } elseif ($item['type'] == 'textarea') {
                $answer = array_merge($answer, ['textarea' => $value]);
            }

            try {
                Answer::create($answer);
            } catch (Exception $e) {
                Log::error($e);
                DB::rollBack();
                return redirect('list_paciente');
            }
        }
        DB::commit();

        return redirect('list_paciente');
    }

    public function list()
    {
        $reports = $this->getReports(Auth::user()->id);

        return view('reports.list', compact('reports'));
    }

    public function listApi(Request $request)
    {
        $userByToken = $request->user();

        $reports = $this->getReports($userByToken['id']);

        return response()->json(['data' => $reports], 200);
    }

    private function getReports($paciente_id)
    {
        $reports = Report::with(['user.attrp.espec', 'answers' => function ($q) {
            $q->whereNotNull('answers.textarea')->OrWhere('answers.checkbox', 1);
        }, 'answers.item'])
        ->where('paciente_id', $paciente_id)->get();

        return $reports;
    }

    public function view($user_id)
    {
        $reports = Report::with(['user.attrp.espec', 'answers' => function ($q) {
            $q->whereNotNull('answers.textarea')->OrWhere('answers.checkbox', 1);
        }, 'answers.item'])
        ->where('paciente_id', $user_id)->get();

        // dd($report);

        return view('reports.view', compact('reports'));
    }
}
