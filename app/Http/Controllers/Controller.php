<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\TokenApi;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function validadeTokenApi($token)
    {
    	$token = str_replace('token ', '', $token);

    	$tokenModel = TokenApi::where("token", $token)->first();

    	return (bool) isset($tokenModel->id);
    }
}
