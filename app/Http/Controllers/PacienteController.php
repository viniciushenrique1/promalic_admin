<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\AttributePaciente;
use Illuminate\Support\Facades\Hash;
use View;

class PacienteController extends Controller
{
    public function save(Request $request)
    {
        $cpf = preg_replace('/[^0-9]/', '', $request->cpf);
        $userCreated = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($cpf),
            'user_type' => 3
        ]);

        AttributePaciente::create([
            'cpf'                 => $cpf,
            'sobrenome'           => $request->sobrenome,
            'user'                => $userCreated->id,
            'endereco'            => $request->endereco,
            'nascimento'          => $request->nascimento,
            'possui_doenca'       => $request->possui_doenca,
            'doenca'              => $request->doenca,
            'com_quem_mora'       => $request->com_quem_mora,
            'com_quem_mora_outro' => $request->com_quem_mora_outro
        ]);

        return redirect()->action('PacienteController@list');
    }

    public function list(Request $request)
    {
        $responseData = [];
            
        $users = User::where('user_type', 3)
        ->join('attribute_pacientes', 'attribute_pacientes.user', '=', 'users.id')
        ->get(['*']);

        foreach ($users as $user) {
            $responseData[] = $user;
        }

        return View::make('list_paciente')->with('users', $responseData);
    }

    public function edit(Request $request)
    {
        $user = User::where('user', $request->id)
        ->join('attribute_pacientes', 'attribute_pacientes.user', '=', 'users.id')
        ->get(['*'])->first();

        return View::make('edit_paciente')->with('user', $user);
    }

    public function save_edit(Request $request)
    {
        $attributePaciente = AttributePaciente::findOrFail($request->attribute_id);

        $attributePaciente->sobrenome = $request->sobrename;
        $attributePaciente->nascimento = $request->nascimento;
        $attributePaciente->endereco = $request->endereco;
        $attributePaciente->possui_doenca = $request->possui_doenca;
        $attributePaciente->doenca = $request->doenca;
        $attributePaciente->com_quem_mora = $request->com_quem_mora;
        $attributePaciente->com_quem_mora_outro = $request->com_quem_mora_outro;

        $attributePaciente->save();

        $user = User::findOrFail($attributePaciente->user);

        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        $user->save();

        return redirect()->action('PacienteController@list');
    }

    public function delete(Request $request)
    {
        $user = User::find($request->user_id);
        $user->delete();

        $attributePaciente = AttributePaciente::findOrFail($request->attributes_id);
        $attributePaciente->delete();

        return redirect()->action('PacienteController@list');
    }
}
