<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\AttributeProfissional;
use Illuminate\Support\Facades\Hash;

class ProfissionaisController extends Controller
{
    public function list(Request $request)
    {
    	$token = $request->header('Authorization');

    	if ($this->validadeTokenApi($token)) {

    		$responseData = [];
    		
    		$users = User::where('user_type', 2)
		    ->join('attribute_profissionals', 'attribute_profissionals.user', '=', 'users.id')
		    ->get(['*']);

    		foreach ($users as $user) {
    			$responseData[] = [
    				'id' => $user->id,
    				'email' => $user->email,
    				'name' => $user->name,
    				'sobrenome' => $user->sobrenome,
    				'especialidade' => $user->especialidade,
    				'crm' => $user->crm,
    				'unidade_saude' => $user->unit,
    				'unit_address' => $user->unit_address,
    				'status' => $user->status
    			];
    		}


    		echo json_encode($responseData);
    	} else {
    		echo json_encode(['Não autorizado!']);
    	}
    }

    public function save(Request $request)
    {
        $token = $request->header('Authorization');
        
        if ($this->validadeTokenApi($token)) {

            $userCreated = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'user_type' => 2
        ]);

        AttributeProfissional::create([
            'sobrenome' => $request->sobrenome,
            'user' => $userCreated->id,
            'especialidade' => $request->especialidade,
            'crm' => $request->crm,
            'unit' => $request->unit,
            'unit_address' => $request->unit_address,
            'status' => 0
        ]);
            

            echo json_encode(["profissional" => $userCreated->id]);
        } else {
            echo json_encode(['Não autorizado!']);
        }
    }
}
