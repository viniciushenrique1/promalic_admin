<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Laudo;
use App\AttributePaciente;
use Illuminate\Support\Facades\Hash;

class PacientesController extends Controller
{
    public function list(Request $request)
    {
    	$token = $request->header('Authorization');

    	if ($this->validadeTokenApi($token)) {

    		$responseData = [];

            if ($request->email) {
                $users = User::where('user_type', 3)->where('email', $request->email)
                    ->join('attribute_pacientes', 'attribute_pacientes.user', '=', 'users.id')
                    ->get(['*']);
            } else {
                $users = User::where('user_type', 3)
                    ->join('attribute_pacientes', 'attribute_pacientes.user', '=', 'users.id')
                    ->get(['*']);
            }

    		foreach ($users as $user) {
    			$responseData[] = [
    				'id' => $user->user,
    				'email' => $user->email,
    				'name' => $user->name,
    				'sobrenome' => $user->sobrenome,
    				'nascimento' => $user->nascimento,
    				'endereco' => $user->endereco,
    				'possui_doenca' => $user->possui_doenca,
    				'doenca' => $user->doenca,
    				'com_quem_mora' => $user->com_quem_mora,
    				'com_quem_mora_outro' => $user->com_quem_mora_outro
    			];
    		}


    		echo json_encode($responseData);
    	} else {
    		echo json_encode(['Não autorizado!']);
    	}
    }

    public function save(Request $request)
    {
        $token = $request->header('Authorization');
        
        if ($this->validadeTokenApi($token)) {


            $user = User::where('email', $request->email)
                ->join('attribute_pacientes', 'attribute_pacientes.user', '=', 'users.id')
                ->get(['*'])->first();

            if ((bool)$user) {

                $attributePaciente = AttributePaciente::findOrFail($user->id);

                $attributePaciente->sobrenome = $request->sobrename;
                $attributePaciente->nascimento = $request->nascimento;
                $attributePaciente->endereco = $request->endereco;
                $attributePaciente->possui_doenca = $request->possui_doenca;
                $attributePaciente->doenca = $request->doenca;
                $attributePaciente->com_quem_mora = $request->com_quem_mora;
                $attributePaciente->com_quem_mora_outro = $request->com_quem_mora_outro;

                $attributePaciente->save();

                $user = User::findOrFail($user->user);

                $user->name = $request->name;
                $user->email = $request->email;

                if ($request->password) {
                    $user->password = Hash::make($request->password);
                }

                $user->save();

                echo json_encode(['status' => 'success']);

            } else {
                $userCreated = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'user_type' => 3
                ]);

                AttributePaciente::create([
                    'sobrenome' => $request->sobrename,
                    'user' => $userCreated->id,
                    'endereco' => $request->endereco,
                    'nascimento' => $request->nascimento,
                    'possui_doenca' => $request->possui_doenca,
                    'doenca' => $request->doenca,
                    'com_quem_mora' => $request->com_quem_mora,
                    'com_quem_mora_outro' => $request->com_quem_mora_outro
                ]);

                echo json_encode(["paciente" => $userCreated->id]);
            }
        } else {
            echo json_encode(['Não autorizado!']);
        }
    }

    public function laudo(Request $request) 
    {
        $token = $request->header('Authorization');
        
        if ($this->validadeTokenApi($token)) {

            $laudos = Laudo::where('user', $request->paciente)->get();

            $responseData = [];
            foreach ($laudos as $laudo) {
                $responseData[] = [
                    "id" => $laudo->id,
                    "checkbox" => $laudo->checkbox,
                    "pergunta_1" => $laudo->pergunta_1,
                    "pergunta_2" => $laudo->pergunta_2,
                    "pergunta_3" => $laudo->pergunta_3,
                    "pergunta_4" => $laudo->pergunta_4
                ];
            }

            echo json_encode($responseData);

        } else {
            echo json_encode(['Não autorizado!']);
        }
    }

}
