<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\TokenApi;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function index(Request $request)
    {
    	$user = User::where("email", $request->email)->first();

    	if (Hash::check($request->password, $user->password)) {
    		$token = md5(time() . $request->password );
    		$response = [
    			"token" => $token
    		];

    		$tokenModel = TokenApi::where("user", $user->id)->delete();

    		TokenApi::create([
	            'user' => $user->id,
	            'token' => $token
        	]);
    	} else {
    		$response = ["Não autorizado"];
    	}

        echo json_encode($response);
    }
}
