<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\AttributeProfissional;
use Illuminate\Support\Facades\Hash;
use View;
use App\Models\Especialidade;

class ProfissionalController extends Controller
{
    public function save(Request $request)
    {
        $userCreated = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'user_type' => 2
        ]);

        AttributeProfissional::create([
            'sobrenome' => $request->sobrenome,
            'user' => $userCreated->id,
            'especialidade' => $request->especialidade,
            'crm' => $request->crm,
            'unit' => $request->unit,
            'unit_address' => $request->unit_address,
            'status' => 0
        ]);

        return redirect()->action('ProfissionalController@list');
    }

    public function list(Request $request)
    {
        $responseData = [];
            
        $users = User::where('user_type', 2)
        ->join('attribute_profissionals', 'attribute_profissionals.user', '=', 'users.id')
        ->get(['*']);

        foreach ($users as $user) {
            $responseData[] = $user;
        }

        return View::make('list_professional')->with('users', $responseData);
    }

    public function edit(Request $request)
    {
        $user = User::where('user', $request->id)
        ->join('attribute_profissionals', 'attribute_profissionals.user', '=', 'users.id')
        ->get(['*'])->first();

        return View::make('edit_profissional')->with('user', $user);
    }

    public function save_edit(Request $request)
    {
        $attributeProfissional = AttributeProfissional::findOrFail($request->attribute_id);

        $attributeProfissional->sobrenome = $request->sobrename;
        $attributeProfissional->especialidade = $request->especialidade;
        $attributeProfissional->crm = $request->crm;
        $attributeProfissional->unit = $request->unit;
        $attributeProfissional->unit_address = $request->unit_address;
        $attributeProfissional->status = $request->status;

        $attributeProfissional->save();

        $user = User::findOrFail($attributeProfissional->user);

        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        $user->save();

        return redirect()->action('ProfissionalController@list');
    }

    public function delete(Request $request)
    {
        $user = User::find($request->user_id);
        $user->delete();

        $attributePaciente = AttributeProfissional::findOrFail($request->attributes_id);
        $attributePaciente->delete();

        return redirect()->action('ProfissionalController@list');
    }

    public function new()
    {
        $especialidades = Especialidade::all();

        return view('new_professional', compact('especialidades'));
    }
}
