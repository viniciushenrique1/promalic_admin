<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->user_type == 3) {
            return redirect('report-list');
        } else {
            return view('home');
        }
    }
}
