<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Exception;
use Log;
use Validator;
use App\User;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $input = $request->all();

        $request->validate([
            'email'    => 'required|string|email|max:255',
            'password' => 'required|string|min:8|max:255',
        ]);

        if (!Auth::attempt([ 'email' => $input['email'], 'password' => $input['password'] ])) {
            Log::error('Auth failed: ' . $input['email'] . ' - ' . $input['password']);
            return response()->json([
                'status'  => 'false',
                'message' => 'Unauthorized'
            ], 401);
        }

        $user = auth()->user();
        
        // if (!auth()->user()->email_verified_at) {
        //     Log::error('Auth failed (Email not verified): ' . $input['name'] . ' - ' . $input['password']);
        //     return response()->json([
        //         'status'  => 'false',
        //         'message' => 'Unauthorized'
        //     ], 401);
        // }

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();

        Log::info('(Login) Auth OK: ' . $user['id'] . ' - ' . $user['name']);

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
            'user'         => $user
        ], 200);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json('Ok', 200);
    }
}
