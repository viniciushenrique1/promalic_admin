<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributePaciente extends Model
{
    protected $fillable = [
        'cpf', 'sobrenome', 'user', 'nascimento', 'endereco', 'possui_doenca', 'doenca', 'com_quem_mora','com_quem_mora_outro'
    ];
}
